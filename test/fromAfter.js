import test from 'ava'
import fromAfter from '..'

test('chop after text', (t) => {
  t.is('foobar'::fromAfter('foo'), 'bar')
})

test('chop after emoji', (t) => {
  t.is('foo💩bar'::fromAfter('💩'), 'bar')
})
